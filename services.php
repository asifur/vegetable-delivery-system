<?php 
$page = "services";
include "includes/header.php"; ?>    

<section class="main-slider">
        <div class="slider-item">
            <aside class="slider-caption">
                <h1><span>Order Now</span>
                    and Get the
                    Farm Fresh
                    Feeling
                </h1>
                <div class="btn-block btn-block--lg">
                    <a href="shop.html#shop-now" id="go-to" title="" class="btn btn-default">Shop Now</a>
                    <a href="#" title="" class="btn btn-orange">See Details</a>
                </div>
            </aside>
            <figure>
                <img src="assets/img/service.jpg" alt="">
            </figure>
        </div>
    </section>
    <!-- End main slider -->
    <section class="delivery-process">
        <div class="wrapper">
            <h2 class="section-title">Order Process</h2>
            <aside class="steps">
                <div class="step">
                    <span>1</span>
                    <h3>Go to Shop</h3>
                    <p>Go to the shop page</p>
                </div>
                <div class="step">
                    <span>2</span>
                    <h3>Select Item</h3>
                    <p>Select Item and add to cart</p>
                </div>
                <div class="step">
                    <span>3</span>
                    <h3>Check Out</h3>
                    <p>Go to cart page and click check out button </p>
                </div>
                <div class="step">
                    <span>4</span>
                    <h3>Click Place Order</h3>
                    <p>Go to check out page, fill up the form and click place order button</p>
                </div>
            </aside>
        </div>
    </section>
    <!-- End delivery process -->
    <section class="order-now">
        <div class="wrapper">
            <h2 class="section-title">Order Us What You Desire </h2>
            <aside class="order-blocks">
                <div class="order-block">
                    <figure>
                        <img src="assets/img/broccoli.png" alt="">
                        <figcaption>
                            Vegetables
                        </figcaption>
                    </figure>
                    <div class="more-order">Discover More</div>
                </div>
                <div class="order-block">
                    <figure>
                        <img src="assets/img/orange.png" alt="">
                        <figcaption>
                            fruits
                        </figcaption>
                    </figure>
                    <div class="more-order">Discover More</div>
                </div>
                <div class="order-block">
                    <figure>
                        <img src="assets/img/chopped-vegetables.jpg" alt="">
                        <figcaption>
                            Chopped Vegetables
                        </figcaption>
                    </figure>
                    <div class="more-order">Discover More</div>
                </div>
            </aside>
            <a class="btn btn-lg btn-default" href="shop.html#shop-now" title="more">Show More</a>
        </div>
    </section>
    <!-- End Order -->
    

<?php include "includes/footer.php"; ?>