<section class="main-slider">
        <div class="slider-item">
            <aside class="slider-caption">
                <h1><span>Order Now</span>
                    and Get the
                    Farm Fresh
                    Feeling
                </h1>
                <div class="btn-block btn-block--lg">
                    <a href="shop.html#shop-now" id="go-to" title="" class="btn btn-default">Shop Now</a>
                    <a href="#" title="" class="btn btn-orange">See Details</a>
                </div>
            </aside>
            <figure>
                <img src="assets/img/main-slider-1.jpg" alt="">
            </figure>
        </div>
    </section>
    <!-- End main slider -->