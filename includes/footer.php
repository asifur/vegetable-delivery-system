<footer class="footer">
        <div class="footer__top">
            <div class="wrapper">
                <div class="row">
                    <div class="col-4">
                        <div class="footer__logo">
                            <a href="#" title="">
                                <img src="assets/img/logo.png" alt="">
                            </a>
                            <p>Fresh fruit and vegetable delivery
                                farms to reduce daily workloads of
                                a busy human.</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="footer__block">
                            <h3>Customer Services</h3>
                            <ul class="footer__nav">
                                <li><a href="support.html" title="">Help Center</a></li>
                                <li><a href="contact.html" title="">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="footer__block">
                            <h3>Our Information</h3>
                            <ul class="footer__nav">
                                <li>Address : </li>
                                <li>Phone No:</li>
                                <li>Email: </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="wrapper">
                <p class="disclaimer">Copyright &copy; 2020-2021 All Rights reserved BUBT</p>
            </div>
        </div>
    </footer>


    <!-- Modal for user input -->
    <div class="custom-modal modal-sign-in">
        <div class="custom-modal-wrap">
            <div class="close-modal">
                <span aria-hidden="true">&times;</span>                
            </div>
            <h2>Sign In</h2>
            <form>
              <div class="form-group">
                <input type="email" placeholder="Email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">                
              </div>
              <div class="form-group">               
                <input type="password" placeholder="password" class="form-control" id="exampleInputPassword1">
              </div>
              <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Save For future</label>
              </div>
              <button type="submit" class="btn btn-orange">Login</button>
            </form>
        </div>
    </div>
    <div class="custom-modal modal-sign-up">
        <div class="custom-modal-wrap">
            <div class="close-modal">
                <span aria-hidden="true">&times;</span>                
            </div>
            <h2>Create an Account</h2>
            <form>
              <div class="form-group">
                <input type="email" placeholder="Username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">                
              </div>

              <div class="form-group">
                <input type="email" placeholder="Email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">                
              </div>
              <div class="form-group">               
                <input type="password" placeholder="password" class="form-control" id="exampleInputPassword1">
              </div>

              <button type="submit" class="btn btn-default">Sign Up</button>
            </form>
        </div>
    </div>

    <div class="custom-modal modal-view-cart">
        <div class="custom-modal-wrap">
            <div class="close-modal">
                <span aria-hidden="true">&times;</span>                
            </div>
            <h2>Cart</h2>
            <div class="row">
                <div class="col-3">
                    <div class="shop-block">
                        <figure>
                            <img src="assets/img/broccoli-md.png" alt="">
                            <figcaption>
                                <h3>Broccoli</h3>
                                <span>100 tk (1 piece)</span>
                                <form>
                                    <div class="value-button decrease" id="decrease" value="Decrease Value">-</div>
                                    <input type="number" id="number" class="total-number" value="0" />
                                    <div class="value-button increase" id="increase" value="Increase Value">+</div>
                                  </form>
                            </figcaption>
                        </figure>
                        
                    </div>
                </div>
                <div class="col-3">
                    <div class="shop-block">
                        <figure>
                            <img src="assets/img/kale.png" alt="">
                            <figcaption>
                                <h3>Kale</h3>
                                <span>100 tk (1 piece)</span>
                                <form>
                                    <div class="value-button decrease" id="decrease" value="Decrease Value">-</div>
                                    <input type="number" id="number" class="total-number" value="0" />
                                    <div class="value-button increase" id="increase" value="Increase Value">+</div>
                                  </form>
                            </figcaption>
                        </figure>
                        
                    </div>
                </div>
                <div class="col-3">
                    <div class="shop-block">
                        <figure>
                            <img src="assets/img/pepper-md.png" alt="">
                            <figcaption>
                                <h3>Pepper</h3>
                                <span>100 tk (1 piece)</span>
                                <form>
                                    <div class="value-button decrease" id="decrease" value="Decrease Value">-</div>
                                    <input type="number" id="number" class="total-number" value="0" />
                                    <div class="value-button increase" id="increase" value="Increase Value">+</div>
                                  </form>
                            </figcaption>
                        </figure>
                        
                    </div>
                </div>
                <div class="col-3">
                    <div class="shop-block">
                        <figure>
                            <img src="assets/img/pepper-md.png" alt="">
                            <figcaption>
                                <h3>Pepper</h3>
                                <span>100 tk (1 piece)</span>
                                <form>
                                    <div class="value-button decrease" id="decrease" value="Decrease Value">-</div>
                                    <input type="number" id="number" class="total-number" value="0" />
                                    <div class="value-button increase" id="increase" value="Increase Value">+</div>
                                  </form>
                            </figcaption>
                        </figure>                        
                    </div>
                </div>
            </div>
            <div class="cost">
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <h5>Sub Total</h5>
                            <h5>Delivery Cost</h5>
                            <h3>Total Cost</h3>
                            
                        </div> 
                        <div class="col-6">
                            <h5>715 tk</h5>
                            <h5>20 tk</h5>
                            <h3>735 tk</h3> 
                        </div>   
                    </div>         
                </div>
            </div> 
            <div class="mx-auto ta-center">
                <a href="#check-out" id="go-to" title="" class="btn btn-lg btn-orange mt-5">Checkout</a>
            </div>
        </div>
    </div>

    <div class="custom-modal modal-check-out" id="check-out">
        <div class="custom-modal-wrap">
            <div class="close-modal">
                <span aria-hidden="true">&times;</span>                
            </div>
            <h2>Check Out</h2>
            <form>
                <h4>Enter Delivery Address</h4>
                <div class="address">
                    <div class="form-group">
                        <input type="text" placeholder="Name" class="form-control" id="exampleInputName">                
                    </div>
                    <div class="form-group">               
                        <input type="text" placeholder="House No." class="form-control" id="exampleInputHouse">
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Street Address" class="form-control" id="exampleInputStreet">                
                    </div>
                    <div class="form-group">               
                        <input type="tel" placeholder="Phone No." class="form-control" id="exampleInputPhone">
                    </div>
                </div>
                <div class="address">
                    <div class="form-group">
                        <input type="date" placeholder="Select Date" class="form-control" id="exampleInputDate">                
                    </div>
                    <div class="form-group">               
                        <input type="time" placeholder="Select Time" class="form-control" id="exampleInputTime">
                    </div>
                    <div class="form-group">
                        <input list="paymentmethod"  placeholder="Select Payment Method" class="form-control" name="paymentmethod">
                        <datalist id="paymentmethod">
                            <option value="Cash">
                            <option value="B-kash">                                   
                            <option value="Visa Card">
                        </datalist>               
                    </div>
                    <div class="form-group">               
                        <input type="text" placeholder="Card Holder Name (Not Required for Cash)" class="form-control" id="exampleInputCHName">
                    </div>
                    <div class="form-group">               
                        <input type="text" placeholder="Card Number (Not Required for Cash)" class="form-control" id="exampleInputCardNumber">
                    </div>
                </div>
                <div class="cost">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <h5>Sub Total</h5>
                                <h5>Delivery Cost</h5>
                                <h3>Total Cost</h3>
                                
                            </div> 
                            <div class="col-6">
                                <h5>715 tk</h5>
                                <h5>20 tk</h5>
                                <h3>735 tk</h3> 
                            </div>   
                        </div>         
                    </div>
                </div> 
                <button type="submit" class="btn btn-orange">Place Order</button>
              </form>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.4/jquery.sticky.min.js" integrity="sha512-QABeEm/oYtKZVyaO8mQQjePTPplrV8qoT7PrwHDJCBLqZl5UmuPi3APEcWwtTNOiH24psax69XPQtEo5dAkGcA==" crossorigin="anonymous"></script>
    <script src="assets/js/init.js"></script>
    
</body>

</html>