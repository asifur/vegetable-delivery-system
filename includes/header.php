<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Vegitable delivery system">
    <title>Home :: Fresh Fruits & Vegi</title>
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="assets/css/styles.css" rel="stylesheet">
</head>

<body>
    <header class="header">
        <div class="wrapper">
            <div class="top-bar">
                <div class="middle">
                    <div class="search">
                        <form action="">
                            <label>
                                <input type="text" placeholder="What are you looking for ?" name="">
                            </label>
                            <button type="submit"><img src="assets/img/magnifier.svg" alt=""></button>
                        </form>
                    </div>
                </div>
                <div class="right">
                    <div class="btn-block credentials-block">
                        <button class="btn btn-orange sign-in">Sign In</button>
                        <button class="btn btn-default sign-up">Sign Up</button>
                    </div>
                </div>
            </div>
            <div class="navbar">
                <div class="left">
                    <a class="logo" href="home.php" title="">
                        <img src="assets/img/logo.png" alt="logo">
                    </a>
                </div>
                <div class="right">
                    <ul class="main-nav">
                        <li class="<?php echo ($page == "home" ? "active" : "")?>"><a href="home.php" title="">Home</a></li>
                        <li class="<?php echo ($page == "services" ? "active" : "")?>"><a href="services.php" title="">Services</a></li>
                        <li class="<?php echo ($page == "about" ? "active" : "")?>"><a href="about.php" title="">About</a></li>
                        <li class="<?php echo ($page == "farming" ? "active" : "")?>"><a href="farming.php" title="">Farming</a></li>
                        <li class="<?php echo ($page == "shop" ? "active" : "")?>"><a href="shop.php" title="">Shop</a></li>
                        <li class="<?php echo ($page == "contact" ? "active" : "")?>"><a href="contact.php" title="">Contacts</a></li>
                    </ul>
                    <div class="dropdown">
                        <div class="menu-cart"><img src="assets/img/circle-cart.svg" alt=""></div>
                        <div class="dropdown-content">
                        <a href="#" class="view-cart">View Cart</a>
                        <a href="#" class="check-out">Check Out</a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </header>
    <!-- End Header -->
