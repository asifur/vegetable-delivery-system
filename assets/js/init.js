  $(document).ready(function(){
    $(".navbar").sticky({
    	topSpacing:0
    });

    
    // Modal triggered for sign in
    $(".sign-in").on("click", function(){
    	$("body").addClass("modal-open-sign-in");
    });
    $(".close-modal").on("click", function(){
    	$("body").removeClass("modal-open-sign-in");
    });

    // Modal triggered for sign up
    $(".sign-up").on("click", function(){
    	$("body").addClass("modal-open-sign-up");
    });
    $(".close-modal").on("click", function(){
    	$("body").removeClass("modal-open-sign-up");
    });

    // Modal triggered for View Cart
    $(".view-cart").on("click", function(){
    	$("body").addClass("modal-open-view-cart");
    });
    $(".close-modal").on("click", function(){
    	$("body").removeClass("modal-open-view-cart");
    });

     // Modal triggered for Check Out
     $(".check-out").on("click", function(){
    	$("body").addClass("modal-open-check-out");
    });
    $(".close-modal").on("click", function(){
    	$("body").removeClass("modal-open-check-out");
    });


    $('#go-to').on('click',function(event){
      var $anchor = $(this);
      $('html, body').animate({
        scrollTop: $($anchor.attr('href')).offset().top + "px"
      }, 1000);
      event.preventDefault();
   });

   function increaseValue() {
    var value = parseInt(document.getElementById('number').value);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('number').value = value;
  }
  function decreaseValue() {
    var value = parseInt(document.getElementById('number').value);
    value = isNaN(value) ? 0 : value;
    value < 1 ? value = 1 : '';
    value--;
    document.getElementById('number').value = value;
  }

   $("#decrease").on("click", function(){
    decreaseValue()    
   });
   $("#increase").on("click", function(){
    increaseValue()
  });    
 
  
   

  });

 